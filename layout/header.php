


<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	<div class="container">
		<a class="navbar-brand" href="index.php"><span class="flaticon-pizza-1 mr-1"></span>CHEZ<br><small>PIZZAIOL'O</small></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="oi oi-menu"></span> Menu
		</button>
		<div class="collapse navbar-collapse" id="ftco-nav">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a href="index.php" class="nav-link">Accueil</a></li>
				<li class="nav-item"><a href="menu.php" class="nav-link">Menu</a></li>
				<!-- <li class="nav-item"><a href="services.php" class="nav-link">Services</a></li> -->
				<!-- <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li> -->
				<li class="nav-item"><a href="inscription.php" class="nav-link">Inscription</a></li>
				<li class="nav-item"><a href="connexion.php" class="nav-link">Connexion</a></li>
				<li class="nav-item"><a href="modifier-profil.php" class="nav-link">Mon compte</a></li>
				<li class="nav-item"><a href="deconnexion.php" class="nav-link">
						<button type="button" class="btn btn-primary mb-2">Déconnexion</button></a></li>
			</ul>
		</div>
	</div>
</nav>
<!-- END nav -->