<?php


try {
    $bdd = new PDO('mysql:host=localhost;dbname=test', 'root', '');
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}
?>
 
<?php
// Effectuer ici la requête qui insère le message
$id_billet = $_POST['id_billet'];
$auteur = htmlspecialchars($_POST['auteur']);
$commentaire = htmlspecialchars($_POST['commentaire']);
$commentaires = $bdd->prepare('INSERT INTO commentaire(id_billet, auteur, commentaire, date_commentaire) VALUES(:id_billet, :auteur, :commentaire, NOW()) ');
$commentaires->execute(array(
    'id_billet' => $id_billet,
    'auteur' => $auteur,
    'commentaire' => $commentaire,
));
header('Location:commentaire.php?id=' . $id_billet . '');

?>
 