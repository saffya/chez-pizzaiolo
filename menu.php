<!DOCTYPE html>

<?php require_once "layout/head.php" ?>

<body>
	<?php require_once "layout/header.php" ?>

	<section class="home-slider owl-carousel img" style="background-image: url(images/bg_1.jpg);">
		<div class="slider-item" style="background-image: url(images/bg_3.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row slider-text justify-content-center align-items-center">

					<div class="col-md-7 col-sm-12 text-center ftco-animate">
						<h1 class="mb-3 mt-5 bread">NOS MENUS</h1>
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-3">
				<div class="col-md-7 heading-section ftco-animate text-center">
					<h2 class="mb-4">NOS PIZZAS VIANDES</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
				</div>
			</div>
		</div>
		<div class="container-wrap">
			<div class="row no-gutters d-flex">
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img" style="background-image: url(images/pizza-1.jpg);"></a>
						<div class="text p-4">
							<h3>Savoyarde</h3>
							<p>Crème fraîche, tomate, mozzarella, lardons, pomme de terre, reblochon </p>
							<p class="price"><span>11,50€</span> </p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img" style="background-image: url(images/pizza-2.jpg);"></a>
						<div class="text p-4">
							<h3>Tartiflette</h3>
							<p>Crème fraîche, mozzarella, jambon, pomme de terre, lardons, reblochon, oignons</p>
							<p class="price"><span>12.90€</span> </p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img" style="background-image: url(images/pizza-3.jpg);"></a>
						<div class="text p-4">
							<h3>Picarde</h3>
							<p>Crème fraîche, 4 fromages, jambon, poulet</p>
							<p class="price"><span>12.90€</span> </p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img order-lg-last" style="background-image: url(images/pizza-5.jpg);"></a>
						<div class="text p-4">
							<h3>Campagnarde</h3>
							<p>Tomate, mozzarella, viande hachée, merguez, œuf, jambon</p>
							<p class="price"><span>14.90€</span> </p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img order-lg-last" style="background-image: url(images/pizza-6.jpg);"></a>
						<div class="text p-4">
							<h3>Basque</h3>
							<p>Tomate, jambon de Bayonne, fromage de brebis, mozzarella et piment d'Espelette</p>
							<p class="price"><span>12.90€</span> </p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img order-lg-last" style="background-image: url(images/pizza-2.jpg);"></a>
						<div class="text p-4">
							<h3>Samouraï</h3>
							<p>Sauce samouraï (légèrement relevée), champignons frais, poulet, pommes de terres, viande hachée, oignons rouge et mozzarella</p>
							<p class="price"><span>13,90€</span> </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-3">
				<div class="col-md-7 heading-section ftco-animate text-center">
					<h2 class="mb-4">NOS PIZZAS POISSONS</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
				</div>
			</div>
		</div>
		<div class="container-wrap">
			<div class="row no-gutters d-flex">
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img" style="background-image: url(images/pizza-1.jpg);"></a>
						<div class="text p-4">
							<h3>Pêcheur</h3>
							<p>Tomate, mozzarella, champignons, thon, artichauts, olives</p>
							<p class="price"><span>11.90€</span> </p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img" style="background-image: url(images/pizza-2.jpg);"></a>
						<div class="text p-4">
							<h3>Napolitaine</h3>
							<p>Tomate, mozzarella, anchois, câpres, olives</p>
							<p class="price"><span>11.90€</span> </p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img" style="background-image: url(images/pizza-3.jpg);"></a>
						<div class="text p-4">
							<h3>Sol del Mar</h3>
							<p>Tomate, mozzarella, anchois, saumons, thons, olives</p>
							<p class="price"><span>$2.90</span> </p>
						</div>
					</div>
				</div>


			</div>
		</div>

	</section>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-3">
				<div class="col-md-7 heading-section ftco-animate text-center">
					<h2 class="mb-4">NOS PIZZAS VEGETARIENNES ET VEGANS</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
				</div>
			</div>
		</div>
		<div class="container-wrap">
			<div class="row no-gutters d-flex">
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img" style="background-image: url(images/pizza-1.jpg);"></a>
						<div class="text p-4">
							<h3>Végétarienne</h3>
							<p>Tomate, mozzarella, champignons, poivrons, artichauts, œuf, olives</p>
							<p class="price"><span>$2.90</span> </p>
						</div>
					</div>
				</div>


				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img order-lg-last" style="background-image: url(images/pizza-4.jpg);"></a>
						<div class="text p-4">
							<h3>Chèvre-miel</h3>
							<p>Tomate, mozzarella, chèvre, noix, miel, crème fraîche </p>
							<p class="price"><span>10.90€</span> </p>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>
	</section>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-3">
				<div class="col-md-7 heading-section ftco-animate text-center">
					<h2 class="mb-4">NOS PIZZAS SUCREES</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
				</div>
			</div>
		</div>
		<div class="container-wrap">
			<div class="row no-gutters d-flex">
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img" style="background-image: url(images/pizza-1.jpg);"></a>
						<div class="text p-4">
							<h3>Nutella</h3>
							<p>Base Nutella, noisettes grillées, morceau de kinder buenos </p>
							<p class="price"><span>$2.90</span> </p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 d-flex ftco-animate">
					<div class="services-wrap d-flex">
						<a href="#" class="img" style="background-image: url(images/pizza-2.jpg);"></a>
						<div class="text p-4">
							<h3>Caramel</h3>
							<p>Base chocolat, amande grillées, coulis de caramel</p>
							<p class="price"><span>$2.90</span> </p>
						</div>
					</div>
				</div>


			</div>
		</div>

	</section>
	<?php require_once "layout/footer.php" ?>