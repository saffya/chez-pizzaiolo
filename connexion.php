<?php require_once "layout/head.php" ?>
<?php

// S'il y a une session alors on ne retourne plus sur cette page  
if (isset($_SESSION['id'])) {
  header('Location: index.php');
  exit;
}

// Si la variable "$_Post" contient des informations alors on les traitres
if (!empty($_POST)) {
  extract($_POST);
  $valid = true;

  if (isset($_POST['connexion'])) {
    $mail = htmlentities(strtolower(trim($mail)));
    $mdp = trim($mdp);

    if (empty($mail)) { // Vérification qu'il y est bien un mail de renseigné
      $valid = false;
      $er_mail = "Il faut mettre un mail";
    }

    if (empty($mdp)) { // Vérification qu'il y est bien un mot de passe de renseigné
      $valid = false;
      $er_mdp = "Il faut mettre un mot de passe";
    }

    // On fait une requête pour savoir si le couple mail / mot de passe existe bien car le mail est unique 
    $req = $DB->query(
      "SELECT * 
                   FROM utilisateur 
                   WHERE mail = ? AND mdp = ?",
      array($mail, crypt($mdp, "$6$rounds=5000$macleapersonnaliseretagardersecret$"))
    );
    $req = $req->fetch();

    // Si on a pas de résultat alors c'est qu'il n'y a pas d'utilisateur correspondant au couple mail / mot de passe
    if ($req['id'] == "") {
      $valid = false;
      $er_mail = "Le mail ou le mot de passe est incorrecte";
    }


    // S'il y a un résultat alors on va charger la SESSION de l'utilisateur en utilisateur les variables $_SESSION
    if ($valid) {
      $_SESSION['id'] = $req['id']; // id de l'utilisateur unique pour les requêtes futures
      $_SESSION['nom'] = $req['nom'];
      $_SESSION['prenom'] = $req['prenom'];
      $_SESSION['mail'] = $req['mail'];

      header('Location: index.php');
      exit;
    }
  }
}
?>



<body>
  <?php require_once "layout/header.php" ?>

  <section class="home-slider owl-carousel img" style="background-image: url(images/bg_1.jpg);">

    <div class="slider-item" style="background-image: url(images/bg_3.jpg);">
      <div class="overlay"></div>
      <div class="container">
        <div class="row slider-text justify-content-center align-items-center">

          <div class="col-md-7 col-sm-12 text-center ftco-animate">
            <h1 class="mb-3 mt-5 bread">Se connecter</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Se connecter</span></p>
          </div>

        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section contact-section">
    <div class="container mt-5">
      <div class="col-md-1"></div>
      <div class="col-md-6 ftco-animate">
        <div class="col-md-12 mb-4">
          <h2 class="h4">Se connecter</h2>
        </div>
        <form action="#" class="contact-form" method="post">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <?php
                if (isset($er_mail)) {
                ?>

                  <div><?= $er_mail ?></div>
                <?php
                }
                ?>
                <input type="text" class="form-control" placeholder="Adresse mail" name="mail" value="<?php if (isset($mail)) {
                                                                                                        echo $mail;
                                                                                                      } ?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <?php
                if (isset($er_mdp)) {
                ?>

                  <div><?= $er_mdp ?></div>
                <?php
                }
                ?>
                <input type="password" class="form-control" placeholder="Mot de passe" name="mdp" value="<?php if (isset($mdp)) {
                                                                                                            echo $mdp;
                                                                                                          } ?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-primary py-3 px-5" name="connexion">
          </div>
        </form>
      </div>
    </div>
    </div>
  </section>

  <?php require_once "layout/footer.php" ?>