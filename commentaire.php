<?php require_once "layout/head.php" ?>

<body>
	<?php require_once "layout/header.php" ?>

	<section class="home-slider owl-carousel img" style="background-image: url(images/bg_1.jpg);">

		<div class="slider-item" style="background-image: url(images/bg_3.jpg);">
			<div class=""></div>
			<div class="container">
				<div class="row slider-text justify-content-center align-items-center">

					<div class="col-md-7 col-sm-12 text-center ftco-animate">
						<h1 class="mb-3 mt-5 bread">Commentaires</h1>
						<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Commentaires</span></p>
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section contact-section">
		<?php
		// Récupération du billet
		$req = $DB->query('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y à %Hh%imin%ss\') AS date_creation_fr FROM billet WHERE id = ?');
		$req->execute(array($_GET['billet']));
		$donnees = $req->fetch();
		?>

		<div class="news">
			<h3>
				<?php echo htmlspecialchars($donnees['titre']); ?>
				<em>le <?php echo $donnees['date_creation_fr']; ?></em>
			</h3>

			<p>
				<?php
				echo nl2br(htmlspecialchars($donnees['contenu']));
				?>
			</p>
		</div>

		<h2>Commentaires</h2>

		<?php
		// Récupération des commentaires du billet
		$req->closeCursor(); // Important : on libère le curseur pour la prochaine requête

		// Récupération des commentaires
		$req = $DB->query('SELECT auteur, commentaire, DATE_FORMAT(date_commentaire, \'%d/%m/%Y à %Hh%imin%ss\') AS date_commentaire_fr FROM commentaire WHERE id_billet = ? ORDER BY date_commentaire');
		$req->execute(array($_GET['billet']));

		while ($donnees = $req->fetch()) {
		?>
			<p><strong><?php echo htmlspecialchars($donnees['auteur']); ?></strong> le <?php echo $donnees['date_commentaire_fr']; ?></p>
			<p><?php echo nl2br(htmlspecialchars($donnees['commentaire'])); ?></p>
		<?php
		} // Fin de la boucle des commentaires
		$req->closeCursor();


		// formulaire de commentaire

		?>
		<p>
		<form method="post" action="commentaire_post.php">

			<input type="hidden" name='id_billet' value="<?php echo $_GET['id']; ?>" />

			Pseudo : <input type="text" name="auteur" value="Identifiant" /><br />

			<textarea name="commentaire" rows="8" cols="45">
	 Votre texte ici
 </textarea><br />

			<p>
				<input type="submit" value="Valider" />
			</p>

		</form>
		</p>


	</section>

	<?php require_once "layout/footer.php" ?>