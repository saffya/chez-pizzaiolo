<?php require_once "layout/head.php" ?>
<?php


// S'il y a une session alors on ne retourne plus sur cette page
if (isset($_SESSION['id'])) {
   header('Location: index.php');
   exit;
}

// Si la variable "$_Post" contient des informations alors on les traitres
if (!empty($_POST)) {
   extract($_POST);
   $valid = true;

   // On se place sur le bon formulaire grâce au "name" de la balise "input de mon bouton de validation"
   if (isset($_POST['inscription'])) {
      $nom  = htmlentities(trim($nom)); // On récupère le nom , le trim enleves les espaces avant et apres
      $prenom = htmlentities(trim($prenom)); // on récupère le prénom
      $mail = htmlentities(strtolower(trim($mail))); // On récupère le mail & avec strtolower les caractères sont en minuscules
      $mdp = trim($mdp); // On récupère le mot de passe 
      $confmdp = trim($confmdp); //  On récupère la confirmation du mot de passe

      //  Vérification du nom
      if (empty($nom)) {
         $valid = false;
         $er_nom = ("Le nom d' utilisateur ne peut pas être vide");
      }

      //  Vérification du prénom
      if (empty($prenom)) {
         $valid = false;
         $er_prenom = ("Le prenom d' utilisateur ne peut pas être vide");
      }

      // Vérification du mail
      if (empty($mail)) {
         $valid = false;
         $er_mail = "Le mail ne peut pas être vide";

         // On vérifit que le mail est dans le bon format
      } elseif (!preg_match("/^[a-z0-9\-_.]+@[a-z]+\.[a-z]{2,3}$/i", $mail)) {
         $valid = false;
         $er_mail = "Le mail n'est pas valide";
      } else {
         // On vérifit que le mail est disponible
         $req_mail = $DB->query(
            "SELECT mail FROM utilisateur WHERE mail = ?",
            array($mail)
         );

         $req_mail = $req_mail->fetch();

         if ($req_mail['mail'] <> "") {
            $valid = false;
            $er_mail = "Ce mail existe déjà";
         }
      }

      // Vérification du mot de passe
      if (empty($mdp)) {
         $valid = false;
         $er_mdp = "Le mot de passe ne peut pas être vide";
      } elseif ($mdp != $confmdp) {
         $valid = false;
         $er_mdp = "La confirmation du mot de passe ne correspond pas";
      }

      // Si toutes les conditions sont remplies alors on fait le traitement
      if ($valid) {

         $mdp = crypt($mdp, "$6$rounds=5000$macleapersonnaliseretagardersecretkgdjdhgvhfx$");
         $date_creation_compte = date('Y-m-d H:i:s');

         // On insert nos données dans la table utilisateur
         $DB->insert(
            "INSERT INTO utilisateur (nom, prenom, mail, mdp, date_creation_compte) VALUES 
                   (?, ?, ?, ?, ?)",
            array($nom, $prenom, $mail, $mdp, $date_creation_compte)
         );

         header('Location: index.php');
         exit;
      }
   }
}
?>



<body>
   <?php require_once "layout/header.php" ?>
   <section class="home-slider owl-carousel img" style="background-image: url(images/bg_1.jpg);">
      <div class="slider-item" style="background-image: url(images/bg_3.jpg);">
         <div class="overlay"></div>
         <div class="container">
            <div class="row slider-text justify-content-center align-items-center">
               <div class="col-md-7 col-sm-12 text-center ftco-animate">
                  <h1 class="mb-3 mt-5 bread">S'inscrire</h1>
                  <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>S'inscrire</span></p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="ftco-section contact-section">
      <div class="container mt-5">
         <div class="col-md-1"></div>
         <div class="col-md-6 ftco-animate">
            <div class="col-md-12 mb-4">
               <h2 class="h4">S'inscrire</h2>
            </div>
            <form action="#" class="contact-form" method="post">
               <div class="row">
                             
                  <div class="col-md-6">
                     <div class="form-group">
                        <?php
                        // S'il y a une erreur sur le nom alors on affiche
                        if (isset($er_nom)) {
                        ?>

                           <div><?= $er_nom ?></div>
                        <?php
                        }
                        ?>

                        <input type="text" class="form-control" placeholder="Prénom" name="nom" value="<?php if (isset($nom)) {
                                                                                                            echo $nom;
                                                                                                         } ?>" required>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <?php
                        if (isset($er_prenom)) {
                        ?>

                           <div><?= $er_prenom ?></div>
                        <?php
                        }
                        ?>
                        <input type="text" class="form-control" placeholder="Nom" name="prenom" value="<?php if (isset($prenom)) {
                                                                                                            echo $prenom;
                                                                                                         } ?>" required>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <?php
                        if (isset($er_mail)) {
                        ?>

                           <div><?= $er_mail ?></div>
                        <?php
                        }
                        ?>
                        <input type="mail" class="form-control" placeholder="Mail" name="mail" value="<?php if (isset($mail)) {
                                                                                                         echo $mail;
                                                                                                      } ?>" required>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <?php
                        if (isset($er_mdp)) {
                        ?>

                           <div><?= $er_mdp ?></div>
                        <?php
                        }
                        ?>
                        <input type="password" class="form-control" placeholder="Mot de passe" name="mdp" value="<?php if (isset($mdp)) {
                                                                                                                     echo $mdp;
                                                                                                                  } ?>" required>
                        <input type="password" class="form-control" placeholder="Confirmer le mot depasse" name="confmdp" required>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <input type="submit" class="btn btn-primary py-3 px-5" name="inscription">
               </div>
            </form>
         </div>
      </div>
      </div>
   </section>
   <?php require_once "layout/footer.php" ?>