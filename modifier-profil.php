<?php require_once "layout/head.php" ?>
<?php

if (!isset($_SESSION['id'])) {
    header('Location: index.php');
    exit;
}

// On récupère les informations de l'utilisateur connecté
$afficher_profil = $DB->query(
    "SELECT * 
           FROM utilisateur 
           WHERE id = ?",
    array($_SESSION['id'])
);
$afficher_profil = $afficher_profil->fetch();

if (!empty($_POST)) {
    extract($_POST);
    $valid = true;

    if (isset($_POST['modification'])) {
        $nom = htmlentities(trim($nom));
        $prenom = htmlentities(trim($prenom));
        $mail = htmlentities(strtolower(trim($mail)));

        if (empty($nom)) {
            $valid = false;
            $er_nom = "Il faut mettre un nom";
        }

        if (empty($prenom)) {
            $valid = false;
            $er_prenom = "Il faut mettre un prénom";
        }

        if (empty($mail)) {
            $valid = false;
            $er_mail = "Il faut mettre un mail";
        } elseif (!preg_match("/^[a-z0-9\-_.]+@[a-z]+\.[a-z]{2,3}$/i", $mail)) {
            $valid = false;
            $er_mail = "Le mail n'est pas valide";
        } else {
            $req_mail = $DB->query(
                "SELECT mail 
                       FROM utilisateur 
                       WHERE mail = ?",
                array($mail)
            );
            $req_mail = $req_mail->fetch();

            if ($req_mail['mail'] <> "" && $_SESSION['mail'] != $req_mail['mail']) {
                $valid = false;
                $er_mail = "Ce mail existe déjà";
            }
        }

        if ($valid) {

            $DB->insert(
                "UPDATE utilisateur SET prenom = ?, nom = ?, mail = ? 
                       WHERE id = ?",
                array($prenom, $nom, $mail, $_SESSION['id'])
            );

            $_SESSION['nom'] = $nom;
            $_SESSION['prenom'] = $prenom;
            $_SESSION['mail'] = $mail;

            header('Location:  profil.php');
            exit;
        }
    }
}
?>



<body>
    <?php require_once "layout/header.php" ?>
    <section class="home-slider owl-carousel img" style="background-image: url(images/bg_1.jpg);">
        <div class="slider-item" style="background-image: url(images/bg_3.jpg);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row slider-text justify-content-center align-items-center">
                    <div class="col-md-7 col-sm-12 text-center ftco-animate">
                        <h1 class="mb-3 mt-5 bread">Mon compte</h1>
                        <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Mon compte</span></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section contact-section">
        <div class="container mt-5">
            <div class="col-md-1"></div>
            <div class="col-md-12 ftco-animate">
                <div class="col-md-12 mb-4">
                    <h2 class="h4">Mon compte</h2>
                </div>
                <section>
                    <div class="col-md-16">
                        <h2>Vous êtes bien sur le compte de <?= $afficher_profil['nom'] . $afficher_profil['prenom']; ?></h2>
                        <div>Quelques informations sur vous : </div>
                        <ul>
                            <li>Votre id est : <?= $afficher_profil['id'] ?></li>
                            <li>Votre mail est : <?= $afficher_profil['mail'] ?></li>

                            <li>Votre compte a été crée le : <?= $afficher_profil['date_creation_compte'] ?></li>

                        </ul>

                    </div>
                </section>
                <form action="#" class="contact-form" method="post">
                    <div class="row">
                                   
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php
                                if (isset($er_nom)) {
                                ?>

                                    <div><?= $er_nom ?></div>
                                <?php
                                }
                                ?>

                                <input type="text" class="form-control" placeholder="Nom" name="nom" value="<?php if (isset($nom)) {
                                                                                                                echo $nom;
                                                                                                            } else {
                                                                                                                echo $afficher_profil['nom'];
                                                                                                            } ?>">


                                <?php
                                if (isset($er_prenom)) {
                                ?>

                                    <div><?= $er_prenom ?></div>
                                <?php
                                }
                                ?>
                                <input type="text" class="form-control" placeholder="Prénom" name="prenom" value="<?php if (isset($prenom)) {
                                                                                                                        echo $prenom;
                                                                                                                    } else {
                                                                                                                        echo $afficher_profil['prenom'];
                                                                                                                    } ?>">
                                <?php
                                if (isset($er_mail)) {
                                ?>

                                    <div><?= $er_mail ?></div>
                                <?php
                                }
                                ?>
                                <input type="mail" class="form-control" placeholder="Mail" name="mail" value="<?php if (isset($mail)) {
                                                                                                                    echo $mail;
                                                                                                                } else {
                                                                                                                    echo $afficher_profil['mail'];
                                                                                                                } ?>">
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary py-3 px-5" name="modification">
                    </div>
                </form>
            </div>
        </div>
        </div>
    </section>
    <?php require_once "layout/footer.php" ?>