
<?php require_once "layout/head.php" ?>


<body>
	<?php require_once "layout/header.php" ?>

	<section class="home-slider owl-carousel img" style="background-image: url(images/bg_1.jpg);">
		<div class="slider-item">
			<div class="overlay"></div>
			<div class="container">
				<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

					<div class="col-md-7 col-sm-12 text-center ftco-animate">
						<span class="subheading">Bienvenue</span>
						<h1 class="mb-4">Vos pizzas préférées, au meilleur prix</h1>
						<p class="mb-4 mb-md-5">13 pizzas, cuites au feu de bois, salées ou sucrées.</p>
						<p> <a href="menu.php" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Voir le menu</a></p>
					</div>

				</div>
			</div>
		</div>
		<div class="slider-item">
			<div class="overlay"></div>
			<div class="container">
				<div class="row slider-text align-items-center" data-scrollax-parent="true">

					<div class="col-md-6 col-sm-12 ftco-animate">
						<span class="subheading">Salées</span>
						<h1 class="mb-4">A l'ancienne</h1>
						<p> <a href="menu.php" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Voir le menu</a></p>
					</div>
					<div class="col-md-6 ftco-animate">
						<img src="images/bg_1.png" class="img-fluid" alt="">
					</div>

				</div>
			</div>
		</div>

		<div class="slider-item">
			<div class="overlay"></div>
			<div class="container">
				<div class="row slider-text align-items-center" data-scrollax-parent="true">

					<div class="col-md-6 col-sm-12 order-md-last ftco-animate">
						<span class="subheading">Sucrées</span>
						<h1 class="mb-4">Une dose de nouveauté</h1>
						<p> <a href="menu.php" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Voir le menu</a></p>
					</div>
					<div class="col-md-6 ftco-animate">
						<img src="images/pizza_index.png" class="img-fluid" alt="">
					</div>

				</div>
			</div>
		</div>


	</section>

	<!--  Informations rapides-->
	<section class="ftco-intro">
		<div class="container-wrap">
			<div class="wrap d-md-flex">
				<div class="info">
					<div class="row no-gutters">
						<div class="col-md-4 d-flex ftco-animate">
							<div class="icon"><span class="icon-phone"></span></div>
							<div class="text">
								<h3>06 50 75 80 75</h3>
								<p>Une petite rivière nommée l'Angaut coule à coté</p>
							</div>
						</div>
						<div class="col-md-4 d-flex ftco-animate">
							<div class="icon"><span class="icon-my_location"></span></div>
							<div class="text">
								<h3>7 rue des boucheries</h3>
								<p>Un sublime restaurant au charme italien</p>
							</div>
						</div>
						<div class="col-md-4 d-flex ftco-animate">
							<div class="icon"><span class="icon-clock-o"></span></div>
							<div class="text">
								<h3>Ouvert Lundi-Samedi</h3>
								<p>8:00 - 22:00 </p>
							</div>
						</div>
					</div>
				</div>
				<div class="social d-md-flex pl-md-5 p-4 align-items-center">
					<ul class="social-icon">
						<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
						<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
						<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<!-- Introduction & accueil-->
	<section class="ftco-about d-md-flex">
		<div class="one-half img" style="background-image: url(images/about.jpg);"></div>
		<div class="one-half ftco-animate">
			<div class="heading-section ftco-animate ">
				<h2 class="mb-4">Bienvenue Chez Pizza<span class="flaticon-pizza">iol'o</span></h2>
			</div>
			<div>
				<p>Dans un joli village du Puy-de-Dome, à quelques minutes seulement de l'A75, le charme champêtre du restaurant Chez Pizzaiol'o vous attend. Venez vous détendre dans cet établissement de qualité, tenu par des propriétaires primés qui ont su allier modernité et rusticité. Le poêle à bois est idéal pour se réchauffer les soirées d'hiver tandis que la terrasse avec vue sur la campagne ou la jolie cour fleurie seront parfaites pour profiter des mois d'été. Le restaurant propose un menu quotidien à base de produit frais pour des prix allant de 11,50€ à 19,50€. Toutes les pizzas étant préparée sur place dans la cuisine du restaurant, tout régime alimentaire particulier peut être pris en compte. La plupart des sauces et pates sont toutes sans glutens. </p>
			</div>
		</div>
	</section>
	<section class="ftco-section ftco-services">
		<div class="overlay"></div>
		<div class="container">
			<div class="row justify-content-center mb-5 pb-3">
				<div class="col-md-7 heading-section ftco-animate text-center">
					<h2 class="mb-4">Nos services</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 ftco-animate">
					<div class="media d-block text-center block-6 services">
						<div class="icon d-flex justify-content-center align-items-center mb-5">
							<span class="flaticon-diet"></span>
						</div>
						<div class="media-body">
							<h3 class="heading">Healthy Foods</h3>
							<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 ftco-animate">
					<div class="media d-block text-center block-6 services">
						<div class="icon d-flex justify-content-center align-items-center mb-5">
							<span class="flaticon-bicycle"></span>
						</div>
						<div class="media-body">
							<h3 class="heading">Livraison rapide</h3>
							<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 ftco-animate">
					<div class="media d-block text-center block-6 services">
						<div class="icon d-flex justify-content-center align-items-center mb-5"><span class="flaticon-pizza-1"></span></div>
						<div class="media-body">
							<h3 class="heading">Recettes originales</h3>
							<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Des chiffres et des nombres , récompenses-->
	<section class="ftco-counter ftco-bg-dark img" id="section-counter" style="background-image: url(images/bg_2.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<div class="row">
						<div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
							<div class="block-18 text-center">
								<div class="text">
									<div class="icon"><span class="flaticon-pizza-1"></span></div>
									<strong class="number" data-number="12">0</strong>
									<span>Franchises</span>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
							<div class="block-18 text-center">
								<div class="text">
									<div class="icon"><span class="flaticon-medal"></span></div>
									<strong class="number" data-number="27">0</strong>
									<span>Nombres de récompenses</span>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
							<div class="block-18 text-center">
								<div class="text">
									<div class="icon"><span class="flaticon-laugh"></span></div>
									<strong class="number" data-number="10567">0</strong>
									<span>Clients satisfait</span>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
							<div class="block-18 text-center">
								<div class="text">
									<div class="icon"><span class="flaticon-chef"></span></div>
									<strong class="number" data-number="45">0</strong>
									<span>Staff</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- Commentaires & avis 3-->
	<section class="ftco-section ftco-services">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-3">
				<div class="col-md-7 heading-section ftco-animate text-center">
					<h2 class="mb-4">Les commentaires et avis clients ! </h2>
					<p>N'hésitez pas à laisser un avis sur nos prestations. Nous aimons vous satisfaire, nous prenons en compte toutes vos remontées</p>
				</div>
			</div>
			<?php
			// On récupère les 5 derniers billets
			$req = $DB->query('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y à %Hh%imin%ss\') AS date_creation_fr FROM billet ORDER BY date_creation DESC LIMIT 0, 5');

			while ($donnees = $req->fetch()) {
			?>
				<div>
					<h3>
						<?php echo htmlspecialchars($donnees['titre']); ?>
						<em>le <?php echo $donnees['date_creation_fr']; ?></em>
					</h3>

					<p>
						<?php
						// On affiche le contenu du billet
						echo nl2br(htmlspecialchars($donnees['contenu']));
						?>
						<br />
						<em><a color="primary" href="commentaire.php?billet=<?php echo $donnees['id']; ?>">Commentaires</a></em>
					</p>
				</div>
			<?php
			} // Fin de la boucle des billets
			$req->closeCursor();
			?>

		</div>
	</section>

	<!-- Contact-->
	<section class="ftco-section contact-section">
		<div class="container mt-5">
			<div class="row block-9">
				<div class="col-md-4 contact-info ftco-animate">
					<div class="row">
						<div class="col-md-12 mb-4">
							<h2 class="h4">Contact Information</h2>
						</div>
						<div class="col-md-12 mb-3">
							<p><span>Adresse</span> 7 rue des boucheries 63160 BILLOM</p>
						</div>
						<div class="col-md-12 mb-3">
							<p><span>Téléphone:</span> <a href="tel://1234567920">06 50 75 80 75</a></p>
						</div>
						<div class="col-md-12 mb-3">
							<p><span>Email:</span> <a href="mailto:info@yoursite.com">goncalves.saffya@gmail.com</a></p>
						</div>
						<div class="col-md-12 mb-3">
							<p><span>Site web:</span> <a href="#"> https://saffya-goncalves.fr/chezpizzaiolos/</a></p>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-6 ftco-animate">
					<form action="#" class="contact-form">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nom">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Objet">
						</div>
						<div class="form-group">
							<textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
						</div>
						<div class="form-group">
							<input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<?php require_once "layout/footer.php" ?>